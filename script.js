new WOW().init();

var mainSlider = new Swiper(".main-slider", {
  effect: "fade",
  loop: true,
  slidesPerView: "auto",
  autoplay: {
    delay: 3000,
    disableOnInteraction: false,
  },
  speed: 2000,
});

var programsSlider = new Swiper(".programs-slider", {
  loop: true,
  spaceBetween: 20,
  slidesPerView: "auto",
  autoplay: {
    delay: 5000,
    disableOnInteraction: false,
  },
  speed: 2000,
});

var salesSlider = new Swiper(".sales-slider", {
  loop: true,
  spaceBetween: 20,
  slidesPerView: 1,
  speed: 2000,
  autoplay: {
    delay: 8000,
    disableOnInteraction: false,
  },
  navigation: {
    nextEl: ".arrow-next",
    prevEl: ".arrow-prev",
  },
});

window.addEventListener('resize', function() {
  const windowInnerWidth = document.documentElement.clientWidth;

  if (windowInnerWidth <= 767) { 
    var reviewsSlider = new Swiper(".reviews-slider", {
      loop: true,
      spaceBetween: 30,
      slidesPerView: 1,
      speed: 2000,
      autoplay: {
        delay: 8000,
        disableOnInteraction: false,
      },
    });

  } 
});

function goToTop() {
  window.scrollTo({
    top: 0,
    duration: 3000,
    behavior: 'smooth',
  });
}

function menu() {
  const menuElements = document.querySelectorAll('.burger-button, .header-menu');
  menuElements.forEach(element => {
    element.classList.toggle('isOpen');
  })
}




